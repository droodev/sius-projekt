package clumaster;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.generated.StormTopology;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.utils.Utils;
import clumaster.bolting.FileSaverGlobalBolt;
import clumaster.bolting.LocalFPBolt;
import clumaster.bolting.PointDispenserBolt;
import clumaster.bolting.TimekeeperBolt;
import clumaster.bolting.commons.ClumasterStreamConfiguration;
import clumaster.bolting.commons.ClumasterStreams;
import clumaster.generator.CenterPointGenerator;
import clumaster.generator.PointGeneratorBuilder;
import clumaster.generator.numbers.ComparableFloat;
import clumaster.generator.numbers.ComparableInteger;
import clumaster.generator.numbers.ComparableNumber;
import clumaster.generator.numbers.LeftClosedRightOpenRange;
import clumaster.model.NDPointBuilder;
import clumaster.model.Point;
import clumaster.spouting.GeneratorSpout;

@SuppressWarnings("unused")
public class Starter {

	private static final String OUTPUT_DIR_KEY = "outputDir";

	private static final int AROUND_CENTER_GENERATOR_MAX_DISTANCE = 50;

	private static final int POINT_DIMENSION = 24;

	public static final List<Point<Integer>> CHOSEN_CENTERS_INT = Arrays
			.asList(NDPointBuilder.<Integer> create(POINT_DIMENSION)
					.setCoordinate(0, 1000).setCoordinate(1, 1000)
					.setCoordinate(2, 1000).setCoordinate(3, 1000).build(),
					NDPointBuilder.<Integer> create(POINT_DIMENSION)
							.setCoordinate(0, 2000).setCoordinate(1, 2000)
							.setCoordinate(2, 2000).setCoordinate(3, 2000)
							.build(),
					NDPointBuilder.<Integer> create(POINT_DIMENSION)
							.setCoordinate(0, 1090).setCoordinate(1, 1000)
							.setCoordinate(2, 1000).setCoordinate(3, 1000)
							.build(),
					NDPointBuilder.<Integer> create(POINT_DIMENSION)
							.setCoordinate(0, 2000).setCoordinate(1, 1980)
							.setCoordinate(2, 2000).setCoordinate(3, 2000)
							.build(),
					NDPointBuilder.<Integer> create(POINT_DIMENSION)
							.setCoordinate(0, 3000).setCoordinate(1, 3000)
							.setCoordinate(2, 3000).setCoordinate(3, 3000)
							.build());

	public static final List<Point<Float>> CHOSEN_CENTERS_FLOAT = new ArrayList<Point<Float>>();

	public static final int CLUSTERING_DEGREE = 6;

	private static final int MIN_POINT_COORD = 0;

	private static final int MAX_POINT_COORD = 3000;

	private static final int POINTS_NUMBER = 15000;

	private static int LOCAL_FP_TASKS = 8;

	private static ArrayList<ArrayList<LeftClosedRightOpenRange<ComparableNumber<Integer>>>> SUBSPACES_INT = new ArrayList<ArrayList<LeftClosedRightOpenRange<ComparableNumber<Integer>>>>();

	private static ArrayList<ArrayList<LeftClosedRightOpenRange<ComparableNumber<Float>>>> SUBSPACES_FLOAT;

	private static final String EPS_KEY = "eps";

	private static final String NUM_NODES = "numNodes";

	private static final String STARTING_MODE_KEY = "startLocally";

	private static final Map<Integer, List<Integer>> SUBSPACES_FOR_LOCALFP_NUM = new HashMap<Integer, List<Integer>>();
	
	private static String outputDir = "out.out";

	static {
		// buildSubspacesInt();

		SUBSPACES_FOR_LOCALFP_NUM.put(2, Arrays.asList(2));
		SUBSPACES_FOR_LOCALFP_NUM.put(3, Arrays.asList(3));
		SUBSPACES_FOR_LOCALFP_NUM.put(4, Arrays.asList(2, 2));
		SUBSPACES_FOR_LOCALFP_NUM.put(6, Arrays.asList(3, 2));
		SUBSPACES_FOR_LOCALFP_NUM.put(8, Arrays.asList(2, 2, 2));
		SUBSPACES_FOR_LOCALFP_NUM.put(9, Arrays.asList(3, 3));
		SUBSPACES_FOR_LOCALFP_NUM.put(12, Arrays.asList(3, 2, 2));

	}
	
	public static void main(String[] args) {

		float eps = 1.5F;
		int numNodes = 2;
		String propsFile;
		boolean startLocally = true;

		if (args.length > 0) {
			propsFile = args[0];

			Properties props = new Properties();
			try {
				InputStream in = Starter.class.getClassLoader()
						.getResourceAsStream(propsFile);
				props.load(in);
				in.close();
				eps = Float.parseFloat(props.getProperty(EPS_KEY));
				numNodes = Integer.parseInt(props.getProperty(NUM_NODES));
				startLocally = Boolean.parseBoolean(props
						.getProperty(STARTING_MODE_KEY));
				outputDir = props.getProperty(OUTPUT_DIR_KEY);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		System.out.println(eps + " " + numNodes);

		LOCAL_FP_TASKS = numNodes;

		List<Integer> subspacesDivisions = SUBSPACES_FOR_LOCALFP_NUM
				.get(numNodes);
		SUBSPACES_FLOAT = createSubspacesFloat(subspacesDivisions);

		for (int i = 0; i < CLUSTERING_DEGREE; ++i) {
			NDPointBuilder<Float> builder = NDPointBuilder
					.<Float> create(POINT_DIMENSION);
			for (int j = 0; j < POINT_DIMENSION; ++j) {
				if (j == i) {
					builder.setCoordinate(j, 2000F);
				} else {
					builder.setCoordinate(j, 1000F);
				}
				System.out.println();
			}
			Point<Float> center = builder.build();
			System.out.println(center);
			CHOSEN_CENTERS_FLOAT.add(center);
		}

		CenterPointGenerator<Float> aroundCentersGenerator = PointGeneratorBuilder
				.createWithDimension(POINT_DIMENSION)
				.buildAroundCentersFloatGenerator(
						AROUND_CENTER_GENERATOR_MAX_DISTANCE,
						CHOSEN_CENTERS_FLOAT);

		StormTopology topology = createTopology(new GeneratorSpout<Float>(
				aroundCentersGenerator), eps);

		Config config = createConfig();
		System.out.println("Start");
		if (startLocally) {
			submitLocally(topology, config);
		} else {
			submitToNimbus(topology, config);
		}
		System.out.println("Starter ended gently");
	}

	public static String getOutputDir(){
		return outputDir;
	}
	
	private static void submitToNimbus(StormTopology topology, Config config) {
		try {
			StormSubmitter.submitTopology("mytopology", config, topology);
		} catch (AlreadyAliveException | InvalidTopologyException e) {
			e.printStackTrace();
		}
	}

	private static void submitLocally(StormTopology topology, Config config) {
		String clusterName = "test";
		LocalCluster cluster = start(topology, config, clusterName);
		Utils.sleep(60000);
		end(cluster, clusterName);
	}

	@SuppressWarnings("rawtypes")
	public static <T extends Number> List<Point> getChosenCenters(Class<T> clazz) {
		List<Point> centers = new ArrayList<Point>();
		if (clazz.isAssignableFrom(Integer.class)) {
			centers.addAll(CHOSEN_CENTERS_INT);
			System.out.println("detected int");
			return centers;
		}
		if (clazz.isAssignableFrom(Float.class)) {
			centers.addAll(CHOSEN_CENTERS_FLOAT);
			System.out.println("detected float");
			System.out.println(centers.size());
			return centers;
		}
		System.out.println("detected nothing");
		return centers;
	}

	private static void end(LocalCluster cluster, String clusterName) {
		cluster.killTopology(clusterName);
		cluster.shutdown();
	}

	private static LocalCluster start(StormTopology topology, Config config,
			String clusterName) {
		LocalCluster cluster = new LocalCluster();
		cluster.submitTopology(clusterName, config, topology);
		return cluster;
	}

	@SuppressWarnings({ "unchecked" })
	private static <T extends Number> StormTopology createTopology(
			GeneratorSpout<T> emittingSpout, float eps) {
		TopologyBuilder builder = new TopologyBuilder();

		String tupleGeneratorID = "generator";
		String timekeeperID = "timekeeper";
		String localCluisteringComponent = "localClus";
		String dispID = "disp";

		builder.setSpout(tupleGeneratorID, emittingSpout, 1);

		builder.setBolt(
				timekeeperID,
				new TimekeeperBolt<T>(
						POINTS_NUMBER,
						Arrays.asList(
								ClumasterStreamConfiguration
										.createIndirectStreamConfiguration(ClumasterStreams.DELETE),
								ClumasterStreamConfiguration
										.createIndirectStreamConfiguration(ClumasterStreams.INSERT))),
				1).shuffleGrouping(tupleGeneratorID,
				ClumasterStreams.INSERT.toString());

		builder.setBolt(
				dispID,
				new PointDispenserBolt<Number>(
						Arrays.asList(
								ClumasterStreamConfiguration
										.createIndirectStreamConfiguration(ClumasterStreams.DELETE),
								ClumasterStreamConfiguration
										.createIndirectStreamConfiguration(ClumasterStreams.INSERT)),
						(List<? extends List<LeftClosedRightOpenRange<ComparableNumber<Number>>>>) SUBSPACES_FLOAT),
				1)
				.directGrouping(timekeeperID,
						ClumasterStreams.DELETE.toString())
				.shuffleGrouping(timekeeperID,
						ClumasterStreams.INSERT.toString());

		builder.setBolt(
				localCluisteringComponent,
				new LocalFPBolt<>(
						Float.class,
						Arrays.asList(ClumasterStreamConfiguration
								.createIndirectStreamConfiguration(ClumasterStreams.CLUSTERINGS)),
						eps), LOCAL_FP_TASKS)
				.directGrouping(dispID, ClumasterStreams.INSERT.toString())
				.directGrouping(dispID, ClumasterStreams.DELETE.toString());

		builder.setBolt(
				"out",
				new FileSaverGlobalBolt<Float>(Float.class, "testLocalFPTasks"
						+ LOCAL_FP_TASKS + ".txt",
						getChosenCenters(Float.class))).shuffleGrouping(
				localCluisteringComponent,
				ClumasterStreams.CLUSTERINGS.toString());

		return builder.createTopology();
	}

	private static ArrayList<ArrayList<LeftClosedRightOpenRange<ComparableNumber<Float>>>> createSubspacesFloat(
			List<Integer> subspacesDivisions) {
		ArrayList<ArrayList<LeftClosedRightOpenRange<ComparableNumber<Float>>>> subspaces = new ArrayList<ArrayList<LeftClosedRightOpenRange<ComparableNumber<Float>>>>();
		for (int i = 0; i < LOCAL_FP_TASKS; ++i) {
			subspaces
					.add(new ArrayList<LeftClosedRightOpenRange<ComparableNumber<Float>>>());
		}

		for (int i = 0; i < LOCAL_FP_TASKS; ++i) {
			ArrayList<LeftClosedRightOpenRange<ComparableNumber<Float>>> subspace = subspaces
					.get(i);
			for (int dim = 0; dim < POINT_DIMENSION; ++dim) {
				LeftClosedRightOpenRange<ComparableNumber<Float>> range;
				float step = 0;
				float left, right;
				if (dim < subspacesDivisions.size()) {
					step = (MAX_POINT_COORD - MIN_POINT_COORD)
							/ subspacesDivisions.get(dim);
					int mult = 1;
					for (int j = 0; j < dim; j++) {
						mult *= subspacesDivisions.get(j);
					}
					left = (step * (i / mult)) % MAX_POINT_COORD;
					right = (step * (i / mult) + step) % MAX_POINT_COORD;
					if (right == 0) {
						right = MAX_POINT_COORD;
					}
				} else {
					left = MIN_POINT_COORD;
					right = MAX_POINT_COORD;
				}
				System.out.print("(" + left + "," + right + ") ");
				range = LeftClosedRightOpenRange
						.<ComparableNumber<Float>> define()
						.higherOrEqualThan(ComparableFloat.create(left))
						.lowerThan(ComparableFloat.create(right));
				subspace.add(range);
			}
			System.out.println();
		}
		return subspaces;
	}

	@SuppressWarnings("unused")
	private static void buildSubspacesInt() {
		for (int i = 0; i < LOCAL_FP_TASKS; ++i) {
			SUBSPACES_INT
					.add(new ArrayList<LeftClosedRightOpenRange<ComparableNumber<Integer>>>());
		}
		int step = (MAX_POINT_COORD - MIN_POINT_COORD) / 2;
		for (int i = 0; i < LOCAL_FP_TASKS; ++i) {
			ArrayList<LeftClosedRightOpenRange<ComparableNumber<Integer>>> subspace = SUBSPACES_INT
					.get(i);
			for (int dim = 0; dim < POINT_DIMENSION; ++dim) {
				LeftClosedRightOpenRange<ComparableNumber<Integer>> range;
				if (dim == 0) {
					int left = (step * i) % MAX_POINT_COORD;
					int right = (step * i + step) % MAX_POINT_COORD;
					if (right == 0) {
						right = MAX_POINT_COORD;
					}
					range = LeftClosedRightOpenRange
							.<ComparableNumber<Integer>> define()
							.higherOrEqualThan(
									ComparableInteger.createComparable(left))
							.lowerThan(ComparableInteger.create(right));
				} else if (dim == 1 || dim == 2) {
					int left = (step * (i / (dim * 2))) % MAX_POINT_COORD;
					int right = (step * (i / (dim * 2)) + step)
							% MAX_POINT_COORD;
					if (right == 0) {
						right = MAX_POINT_COORD;
					}
					range = LeftClosedRightOpenRange
							.<ComparableNumber<Integer>> define()
							.higherOrEqualThan(
									ComparableInteger.createComparable(left))
							.lowerThan(ComparableInteger.create(right));
				} else {
					range = LeftClosedRightOpenRange
							.<ComparableNumber<Integer>> define()
							.higherOrEqualThan(
									ComparableInteger.create(MIN_POINT_COORD))
							.lowerThan(
									ComparableInteger.create(MAX_POINT_COORD));
				}

				subspace.add(range);
				System.out.print("(" + range.getLowerBound().getValue() + ", "
						+ range.getUpperBound().getValue() + ") ");
			}
			System.out.println();
		}
	}

	private static Config createConfig() {
		Config conf = new Config();
		conf.setDebug(false);
		conf.setNumWorkers(15);
		return conf;
	}
}
