package clumaster.generator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.base.Preconditions;

import clumaster.model.NDPointBuilder;
import clumaster.model.Point;

/**
 * Generator which aggregates generators of coordinates to generate
 * n-dimensional point.
 * 
 * @param <T> type of generated point coordinates
 */
public class BasePointGenerator<T extends Number> implements
		Generator<Point<T>> {

	private static final long serialVersionUID = 1L;
	private int dimension;
	private List<Generator<T>> generators;

	BasePointGenerator(int dimension, Collection<Generator<T>> generators) {
		this.generators = new ArrayList<>(
				Preconditions.checkNotNull(generators));
		Preconditions
				.checkArgument(
						generators.size() == dimension,
						"Generators list of point with dimension"
								+ " %d must have exactly the same number of elements (has %d)",
						dimension, generators.size());
		this.dimension = dimension;
	}

	/**
	 * @{inheritDoc}
	 */
	@Override
	public Point<T> getNextPoint() {
		NDPointBuilder<T> pointBuilder = NDPointBuilder.<T> create(dimension);
		for (int i = 0; i < dimension; ++i) {
			pointBuilder.setCoordinate(i, generators.get(i).getNextPoint());
		}
		return pointBuilder.build();

	}
}
