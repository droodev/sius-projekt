package clumaster.generator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.google.common.base.Preconditions;

import clumaster.model.Point;

/**
 * Builds points generator which generates points around concrete centers.
 * Coordinates of generated points are going to be within some distance in every
 * coordinate.
 * 
 * @param <T> type of generated point type
 */
@SuppressWarnings("serial")
public class CenterPointGenerator<T extends Number> implements
		Generator<Point<T>> {
	private Random random;
	private List<BasePointGenerator<T>> generators;

	CenterPointGenerator(int dimension,
			Collection<BasePointGenerator<T>> generators) {
		random = new Random(new Date().getTime() ^ (long) hashCode());
		this.generators = new ArrayList<>(
				Preconditions.checkNotNull(generators));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Point<T> getNextPoint() {
		int aroundWhichCenter = random.nextInt(generators.size());
		return generators.get(aroundWhichCenter).getNextPoint();
	}

}
