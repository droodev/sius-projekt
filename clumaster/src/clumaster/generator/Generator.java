package clumaster.generator;

import java.io.Serializable;


/**
 * Represents generator which is capable of creating points. 
 *
 * @param <T> type of point
 */
public interface Generator<T> extends Serializable{
	T getNextPoint();
}
