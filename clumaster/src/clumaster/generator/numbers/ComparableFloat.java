package clumaster.generator.numbers;

import com.google.common.base.Preconditions;
/**
 * {@link ComparableNumber} which is intended to wrap {@link Float} {@link Number}.
 *
 */
@SuppressWarnings("serial")
public final class ComparableFloat extends ComparableNumber<Float>{

	private ComparableFloat(Float number) {
		super(number);
	}

	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareTo(ComparableNumber<Float> o) {
		Preconditions.checkNotNull(o);
		return getValue().compareTo(o.getValue());
	}
	
	/**
	 * Factory method.
	 * @param number, float to be represented as {@link ComparableNumber}
	 * @return
	 * comparable version of given number
	 */
	public static ComparableFloat create(Float number){
		return new ComparableFloat(number);
	}

}
