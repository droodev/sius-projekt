package clumaster.generator.numbers;

import java.io.Serializable;

import com.google.common.base.Preconditions;

/**
 * Represents range, as name of class says. Object is light, it's not storing
 * all range values!
 * 
 * @param <T>
 *            type of range elements
 */
@SuppressWarnings("serial")
public class LeftClosedRightOpenRange<T extends ComparableNumber<? extends Number>>
		implements Serializable {

	private T lowerBound;
	private T upperBound;

	private LeftClosedRightOpenRange(T lowerBound, T upperBound) {
		Preconditions.checkArgument(lowerBound.compareTo(upperBound) < 0);
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	/**
	 * Gets lower bound (closed) of this range.
	 * 
	 * @return type-wise lower bound
	 */
	public T getLowerBound() {
		return lowerBound;
	}

	/**
	 * Gets upper bound (open) of this range.
	 * 
	 * @return type-sie upper bound
	 */
	public T getUpperBound() {
		return upperBound;
	}

	/**
	 * Tests if given number falls in the range.
	 * 
	 * @param testedNumber
	 * @return whether number falls in the range
	 */
	public boolean isInRange(T testedNumber) {
		return testedNumber.compareTo(lowerBound) >= 0
				&& testedNumber.compareTo(upperBound) < 0;
	}

	/**
	 * Method which returns builder of the {@link LeftClosedRightOpenRange}
	 * object.
	 * 
	 * @return appropriate builder
	 */
	public static <T extends ComparableNumber<? extends Number>> LeftBuilder<T> define() {
		return new LeftBuilder<T>();
	}

	/**
	 * Builder which accepts left boundary of the range being constructed.
	 * 
	 * @param <T>
	 *            returned range's number type
	 */
	public static class LeftBuilder<T extends ComparableNumber<? extends Number>> {

		private LeftBuilder() {
			// nothing to do
		}

		/**
		 * Fluent API method.
		 * 
		 * @param lowerBound
		 *            of range being constructed
		 * @return builder which expects upperBound
		 */
		public UpperBuilder<T> higherOrEqualThan(T lowerBound) {
			return new UpperBuilder<T>(lowerBound);
		}

	}

	/**
	 * Builder which accepts right boundary of the range being constructed.
	 * 
	 * @param <T>
	 *            returned range's number type
	 */
	public static class UpperBuilder<T extends ComparableNumber<? extends Number>> {

		private T lowerBound;

		private UpperBuilder(T lowerBound) {
			this.lowerBound = lowerBound;
		}

		/**
		 * Fluent API method which eventually builds
		 * {@link LeftClosedRightOpenRange} object.
		 * 
		 * @param upperBound
		 *            of range being constructed (open bound)
		 * @return built range
		 */
		public LeftClosedRightOpenRange<T> lowerThan(T upperBound) {
			return buildRange(upperBound);
		}

		private LeftClosedRightOpenRange<T> buildRange(T upperBound) {
			return new LeftClosedRightOpenRange<T>(lowerBound, upperBound);
		}

	}

}
