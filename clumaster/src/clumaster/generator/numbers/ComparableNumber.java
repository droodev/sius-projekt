package clumaster.generator.numbers;

import java.io.Serializable;

import com.google.common.base.Preconditions;

/**
 * Wrapper which adds capabilities of {@link Comparable} interface to classic {@link Number}
 * class. Feature of comparing {@link Number}s is essential in some use cases of
 * system. Additionally this class adds unified way to get value from number
 * representing class. {@link Number}s do not have unified method to get value -
 * as Number cannot be easily represented as primitive.
 * 
 * @see Number
 * 
 * @param <T>
 *            {@link Number} subtype which is going to be compared
 */
@SuppressWarnings("serial")
public abstract class ComparableNumber<T extends Number> implements
		Comparable<ComparableNumber<T>>, Serializable {

	private final T number;

	protected ComparableNumber(T number) {
		Preconditions.checkNotNull(number);
		this.number = number;
	}

	/**
	 * Unified method to get value of all wrapped {@link Number}. This method
	 * allows for constructions as: <code>
	 * 	this.getValue().compareTo(otherObject.getValue())
	 * </code> which is impossible using only {@link Number} as upper bounding
	 * type. {@link Number}s, are not comparable, however it's subclasses are!F
	 * 
	 * @return type-wise underlying {@link Number} implementation
	 * 
	 */
	public T getValue() {
		return number;
	}

	/**
	 * Simple type-wise method factory of {@link ComparableNumber}.
	 * 
	 * @param number
	 *            to be represented as {@link ComparableNumber}
	 * @return {@link ComparableNumber} appropriate to given {@link Number}'s
	 *         class
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Number> ComparableNumber<T> createComparable(
			T number) {
		Class<? extends Number> clazz = number.getClass();
		if (Integer.class.isAssignableFrom(clazz)) {
			return (ComparableNumber<T>) ComparableInteger.create(Integer.class
					.cast(number));
		} else if (Float.class.isAssignableFrom(clazz)) {
			return (ComparableNumber<T>) ComparableFloat.create(Float.class
					.cast(number));
		} else {
			throw new IllegalArgumentException(String.format(
					"Creating comparable of this type (%s) is not supported",
					clazz));
		}
	}

}
