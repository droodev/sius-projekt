package clumaster.generator.numbers;

import com.google.common.base.Preconditions;

/**
 * {@link ComparableNumber} which is intended to wrap {@link Integer} {@link Number}.
 *
 */
@SuppressWarnings("serial")
public final class ComparableInteger extends ComparableNumber<Integer>{

	private ComparableInteger(Integer number) {
		super(number);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareTo(ComparableNumber<Integer> o) {
		Preconditions.checkNotNull(o);
		return this.getValue().compareTo(o.getValue());
	}
	
	/**
	 * Factory method.
	 * @param number, integer to be represented as {@link ComparableNumber}
	 * @return
	 * comparable version of given number
	 */
	public static ComparableInteger create (Integer number){
		return new ComparableInteger(number);
	}

}
