package clumaster.generator.scalars;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.Random;

import clumaster.generator.numbers.ComparableInteger;
import clumaster.generator.numbers.LeftClosedRightOpenRange;

import com.google.common.base.Preconditions;

/**
 * Generator which generates {@link Integer} numbers according to bounds
 * constraints.
 */
@SuppressWarnings("serial")
public final class IntegerGenerator extends SDKRandomBaseGenerator<Integer> {

	private LeftClosedRightOpenRange<ComparableInteger> range;

	/**
	 * Creates generator which will be producing {@link Integer} in given
	 * {@link LeftClosedRightOpenRange} seeding it with given value.
	 * 
	 * @param seed
	 *            to initialize {@link Random}
	 * @param range
	 *            which created integer will be located in
	 */
	public IntegerGenerator(long seed,
			LeftClosedRightOpenRange<ComparableInteger> range) {
		super(seed);
		this.range = Preconditions.checkNotNull(range);
	}

	/**
	 * Creates generator which will be producing {@link Integer} in given
	 * {@link LeftClosedRightOpenRange}
	 * 
	 * @param range
	 *            which created integer will be located in
	 */
	public IntegerGenerator(LeftClosedRightOpenRange<ComparableInteger> range) {
		this.range = Preconditions.checkNotNull(range);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getNextPoint() {
		double uniformZeroToOneNumber = drawUniformOnZeroToOne();
		while (!isInRightOpen(uniformZeroToOneNumber)) {
			uniformZeroToOneNumber = drawUniformOnZeroToOne();
		}
		BigDecimal boundsRange = calculateBoundsRange();
		return boundsRange.multiply(new BigDecimal(uniformZeroToOneNumber))
				.add(new BigDecimal(range.getUpperBound().getValue()))
				.intValue();
	}

	private boolean isInRightOpen(double uniformZeroToOneNumber) {
		return uniformZeroToOneNumber < 1;
	}

	private BigDecimal calculateBoundsRange() {
		BigDecimal lowerBigDecimal = new BigDecimal(range.getLowerBound()
				.getValue());
		BigDecimal upperBigDecimal = new BigDecimal(range.getUpperBound()
				.getValue());
		return lowerBigDecimal.subtract(upperBigDecimal);
	}

	private double drawUniformOnZeroToOne() {
		byte[] longNumber = new byte[Long.SIZE];
		random.nextBytes(longNumber);
		ByteBuffer wrappingBuffer = ByteBuffer.wrap(longNumber);
		long drawnLong = wrappingBuffer.getLong();
		return Math.abs((double) drawnLong / (Long.MIN_VALUE)); // [0,1] -
																// however
																// bounds are
																// two times
																// less often
																// than other
																// numbers
	}

}
