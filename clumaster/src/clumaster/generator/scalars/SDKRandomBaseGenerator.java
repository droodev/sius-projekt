package clumaster.generator.scalars;

import java.util.Date;
import java.util.Random;

import clumaster.generator.Generator;

/**
 * Base class for generators which are going to use {@link Random} class to generate random numbers.
 * @param <T> type of generated objects
 */
@SuppressWarnings("serial")
public abstract class SDKRandomBaseGenerator<T extends Number> implements Generator<T>{
	protected Random random;


	protected SDKRandomBaseGenerator() {
		random = new Random(new Date().getTime() ^ (long)hashCode());
	}
	
	protected SDKRandomBaseGenerator(long seed){
		random = new Random(seed);
	}


}