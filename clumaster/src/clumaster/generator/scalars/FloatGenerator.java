package clumaster.generator.scalars;

import java.math.BigDecimal;
import java.util.Random;

import clumaster.generator.numbers.ComparableFloat;
import clumaster.generator.numbers.LeftClosedRightOpenRange;

/**
 * Generator which generates {@link Float} numbers according to bounds
 * constraints.
 */
@SuppressWarnings("serial")
public class FloatGenerator extends SDKRandomBaseGenerator<Float> {

	private LeftClosedRightOpenRange<ComparableFloat> range;
	private double drawnDouble;

	/**
	 * Creates generator which will be producing {@link Float} in given
	 * {@link LeftClosedRightOpenRange} seeding it with given value.
	 * 
	 * @param seed
	 *            to initialize {@link Random}
	 * @param range
	 *            which created float will be located in
	 */
	public FloatGenerator(LeftClosedRightOpenRange<ComparableFloat> range) {
		super();
		this.range = range;
	}

	/**
	 * Creates generator which will be producing {@link Float} in given
	 * {@link LeftClosedRightOpenRange}
	 * 
	 * @param range
	 *            which created float will be located in
	 */
	public FloatGenerator(long seed,
			LeftClosedRightOpenRange<ComparableFloat> range) {
		super(seed);
		this.range = range;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Float getNextPoint() {
		drawnDouble = random.nextDouble();
		BigDecimal boundsRange = calculateBoundsRange();
		return boundsRange.multiply(new BigDecimal(drawnDouble))
				.add(new BigDecimal(range.getUpperBound().getValue()))
				.floatValue();
	}

	private BigDecimal calculateBoundsRange() {
		BigDecimal lowerBigDecimal = new BigDecimal(range.getLowerBound()
				.getValue());
		BigDecimal upperBigDecimal = new BigDecimal(range.getUpperBound()
				.getValue());
		return lowerBigDecimal.subtract(upperBigDecimal);
	}
}
