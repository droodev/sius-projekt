package clumaster.generator;

import java.util.ArrayList;
import java.util.Collection;

import clumaster.generator.numbers.ComparableFloat;
import clumaster.generator.numbers.ComparableInteger;
import clumaster.generator.numbers.LeftClosedRightOpenRange;
import clumaster.generator.scalars.FloatGenerator;
import clumaster.generator.scalars.IntegerGenerator;
import clumaster.model.Point;

/**
 * Builder which simplifies creating of available generators.
 * 
 * @param <T>
 *            type of point which is going to be generated via generator built
 *            by this builder
 */
public class PointGeneratorBuilder<T extends Number> {

	private int dimension;

	/**
	 * Builder creation factory method.
	 * 
	 * @param dimension
	 *            of points which are going to be generator constructed of
	 * @return builder
	 */
	public static <T extends Number> PointGeneratorBuilder<T> createWithDimension(
			int dimension) {
		return new PointGeneratorBuilder<T>(dimension);
	}

	private PointGeneratorBuilder(int dimension) {
		this.dimension = dimension;
	}

	/**
	 * Build points generator with given generator for all coordinates of this
	 * point.
	 * 
	 * @param generator
	 *            which is going to be used as generator of one coordinate.
	 *            However this generator will be used for all of point
	 *            coordinates them
	 * @return built generator
	 */
	public BasePointGenerator<T> buildWithSameGenerator(Generator<T> generator) {
		ArrayList<Generator<T>> generatorsList = new ArrayList<>();
		for (int i = 0; i < dimension; ++i) {
			generatorsList.add(generator);
		}
		return new BasePointGenerator<>(dimension, generatorsList);
	}

	/**
	 * Builds points generator which generates float points around given
	 * centers. Coordinates of generated points are going to be within distance
	 * of range in every coordinate.
	 * 
	 * @param range
	 *            which represents each generated point's coordinate from
	 *            appropriate center coordinate, maximum distance
	 * @param points
	 *            centers around which points will be generated
	 * @return built generator
	 */
	public CenterPointGenerator<Float> buildAroundCentersFloatGenerator(
			int range, Collection<Point<Float>> points) {
		Collection<BasePointGenerator<Float>> generators = new ArrayList<>();

		for (Point<Float> point : points) {
			Collection<Generator<Float>> simple_generators = new ArrayList<>();
			for (float coord : point.getCoordinates()) {
				simple_generators
						.add(new FloatGenerator(LeftClosedRightOpenRange
								.<ComparableFloat> define()
								.higherOrEqualThan(
										ComparableFloat.create(coord - range))
								.lowerThan(
										ComparableFloat.create(coord + range))));
			}

			generators.add(new BasePointGenerator<Float>(dimension,
					simple_generators));
		}
		return new CenterPointGenerator<Float>(dimension, generators);
	}

	/**
	 * Builds points generator which generates integer points around given
	 * centers. Coordinates of generated points are going to be within distance
	 * of range in every coordinate.
	 * 
	 * @param range
	 *            which represents each generated point's coordinate from
	 *            appropriate center coordinate maximum distance
	 * @param points
	 *            centers around which points will be generated
	 * @return built generator
	 */
	public CenterPointGenerator<Integer> buildAroundCentersIntegerGenerator(
			int range, Collection<Point<Integer>> points) {
		Collection<BasePointGenerator<Integer>> generators = new ArrayList<>();

		for (Point<Integer> point : points) {
			Collection<Generator<Integer>> simple_generators = new ArrayList<>();
			for (int coord : point.getCoordinates()) {
				simple_generators
						.add(new IntegerGenerator(
								LeftClosedRightOpenRange
										.<ComparableInteger> define()
										.higherOrEqualThan(
												ComparableInteger.create(coord
														- range))
										.lowerThan(
												ComparableInteger.create(coord
														+ range))));
			}

			generators.add(new BasePointGenerator<Integer>(dimension,
					simple_generators));
		}
		return new CenterPointGenerator<Integer>(dimension, generators);
	}
}
