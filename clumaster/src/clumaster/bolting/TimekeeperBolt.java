package clumaster.bolting;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;

import backtype.storm.topology.BasicOutputCollector;
import clumaster.bolting.commons.ClumasterStreamConfiguration;
import clumaster.bolting.commons.ClumasterStreams;
import clumaster.bolting.commons.DeclaringDispatchingBolt;
import clumaster.model.Point;

/**
 * Entry bolt which is responsible for keeping proper number of all points in
 * the system. It stores input points in queue and removes the oldest point if
 * there comes point which would violate number of points system otherwise.
 * 
 * @param <T>
 *            type of point processed by this bolt
 */
@SuppressWarnings("serial")
public class TimekeeperBolt<T extends Number> extends
		DeclaringDispatchingBolt<T> {

	private int numberOfActivePoints;
	private int maxNumberOfPoints;
	private ArrayDeque<PointReceiverPair> pointDeque;

	/**
	 * Constructs this bolt with given capacity of points.
	 * 
	 * @see SimpleStreamDispatchingBolt
	 * @param maxNumberOfPoints
	 *            which could be taken into account while counting clustering
	 * @param configuration
	 *            of streams to which this bolt will be processing tuples
	 */
	public TimekeeperBolt(int maxNumberOfPoints,
			List<ClumasterStreamConfiguration> configuration) {
		super(configuration);
		this.maxNumberOfPoints = maxNumberOfPoints;
		pointDeque = new ArrayDeque<TimekeeperBolt<T>.PointReceiverPair>(
				maxNumberOfPoints);
	}

	/**
	 * Simple value storing, immutable class without logic which is intended to
	 * store point and its receiver.
	 */
	private class PointReceiverPair {
		private Point<T> point;
		private Integer taskId;

		/**
		 * Creates new pair point, receiver.
		 * 
		 * @param point
		 *            point which was sent to receiver
		 * @param taskId
		 *            receiver's task id
		 */
		public PointReceiverPair(Point<T> point, Integer taskId) {
			this.point = point;
			this.taskId = taskId;
		}

		/**
		 * Gets point
		 * 
		 * @return stored point
		 */
		public Point<T> getPoint() {
			return point;
		}

		/**
		 * Gets a receiver of the point.
		 * 
		 * @return receiver of the point
		 */
		public Integer getTaskId() {
			return taskId;
		}

	}

	/**
	 * Stores new added point, delete the oldest if it's needed and inform about
	 * decision on the proper stream ({@link ClumasterStreams#INSERT} and
	 * {@link ClumasterStreams#DELETE} respectively).
	 * 
	 * @see clumaster.bolting.BaseSimpleStreamDispatchingBolt#executeInsertStream(clumaster.model.Point,
	 *      backtype.storm.topology.BasicOutputCollector)
	 */
	@Override
	public void executeInsertStream(Point<T> inputPoint,
			BasicOutputCollector collector) {
		if (numberOfActivePoints == maxNumberOfPoints) {
			PointReceiverPair eldestPair = pointDeque.pollLast();
			collector.emitDirect(eldestPair.getTaskId(),
					ClumasterStreams.DELETE.toString(),
					Arrays.<Object> asList(eldestPair.getPoint()));
			--numberOfActivePoints;
		}
		List<Integer> emittedTasks = collector.emit(
				ClumasterStreams.INSERT.toString(),
				Arrays.<Object> asList(inputPoint));
		pointDeque.add(new PointReceiverPair(inputPoint, emittedTasks.get(0)));
		++numberOfActivePoints;

	}

}
