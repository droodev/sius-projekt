package clumaster.bolting.commons;

import java.io.Serializable;

import com.google.common.base.Preconditions;

/**
 * Carries configuration of stream of application. Class is intended to simplify
 * configuration of application topology.
 */
@SuppressWarnings("serial")
public class ClumasterStreamConfiguration implements Serializable {

	/**
	 * Factory method for creating direct stream configuration.
	 * 
	 * @param stream
	 *            which is going to be described by this configuration
	 * @return {@link ClumasterStreamConfiguration} representing direct
	 *         configuration of given stream
	 */
	public static ClumasterStreamConfiguration createDirectStreamConfiguration(
			ClumasterStreams stream) {
		return new ClumasterStreamConfiguration(stream, true);
	}

	/**
	 * Factory method for creating indirect stream configuration.
	 * 
	 * @param stream
	 *            which is going to be described by this configuration
	 * @return {@link ClumasterStreamConfiguration} representing indirect
	 *         configuration of given stream
	 */
	public static ClumasterStreamConfiguration createIndirectStreamConfiguration(
			ClumasterStreams stream) {
		return new ClumasterStreamConfiguration(stream, false);
	}

	private ClumasterStreams stream;
	private boolean isDirect;

	private ClumasterStreamConfiguration(ClumasterStreams stream,
			boolean isDirect) {
		Preconditions.checkNotNull(stream);
		this.stream = stream;
		this.isDirect = isDirect;
	}

	/**
	 * Get target stream of this configuraion.
	 * 
	 * @return target stream which the configuration is bound to
	 */
	public ClumasterStreams getStream() {
		return stream;
	}

	/**
	 * Determines whether stream is going to be configured as direct.
	 * 
	 * @return true if this configuration describes a direct stream
	 */
	public boolean isDirect() {
		return isDirect;
	}

}
