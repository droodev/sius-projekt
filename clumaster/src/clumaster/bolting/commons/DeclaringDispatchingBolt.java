package clumaster.bolting.commons;

import java.util.Collections;
import java.util.List;

import backtype.storm.topology.OutputFieldsDeclarer;
import clumaster.bolting.BaseSimpleStreamDispatchingBolt;

import com.google.common.base.Preconditions;

/**
 * Covers common function of all bolts uses in application. Every bolts is
 * obliged to declare output fields (if wants to emit some tuples).
 * 
 * @param <T>
 *            type of points which will be processed by this bolt
 */
@SuppressWarnings("serial")
public abstract class DeclaringDispatchingBolt<T extends Number> extends
		BaseSimpleStreamDispatchingBolt<T> {
	private final List<ClumasterStreamConfiguration> streamsConfiguration;

	/**
	 * Creates bolt with output fields configured according to given stream
	 * configurations.
	 * 
	 * @see ClumasterStreamConfiguration
	 * 
	 * @param clumasterStreams
	 *            configurations of streams to which this bolt will be producing
	 *            tuples
	 */
	public DeclaringDispatchingBolt(
			List<ClumasterStreamConfiguration> clumasterStreams) {
		Preconditions.checkNotNull(clumasterStreams);
		this.streamsConfiguration = clumasterStreams;
	}

	/**
	 * Creates bolt without any outptu fields.
	 */
	public DeclaringDispatchingBolt() {
		this(Collections.<ClumasterStreamConfiguration> emptyList());
	}

	/**
	 * According to given stream configurations this class declares output
	 * fields. Overriding this method is main cause of creating this class and
	 * helps to utilize DRY rule.
	 * 
	 * @see backtype.storm.topology.IComponent#declareOutputFields(backtype.storm.topology.OutputFieldsDeclarer)
	 */
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		ClumasterStreams stream;
		for (ClumasterStreamConfiguration streamConfiguration : streamsConfiguration) {
			stream = streamConfiguration.getStream();
			declarer.declareStream(stream.toString(),
					streamConfiguration.isDirect(),
					stream.getFieldsOfThisStream());
		}

	}

}
