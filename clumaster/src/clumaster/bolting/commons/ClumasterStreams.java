package clumaster.bolting.commons;

import java.util.ArrayList;

import backtype.storm.tuple.Fields;

/**
 * Contains names and additional helper data for Streams used in application
 * 
 */
public enum ClumasterStreams {
	INSERT("ins", 1), DELETE("del", 1), CLUSTERINGS("clu", 2);

	private String streamName;
	private Fields fieldsOfThisStream;

	private ClumasterStreams(String streamName, int fieldsNumber) {
		this.streamName = streamName;
		fieldsOfThisStream = new Fields(createFieldsList(streamName,
				fieldsNumber));
	}

	private ArrayList<String> createFieldsList(String streamName,
			int fieldsNumber) {
		ArrayList<String> fieldsList = new ArrayList<String>();
		for (int i = 0; i < fieldsNumber; ++i) {
			fieldsList.add(String.format("%s%d", streamName, i));
		}
		return fieldsList;
	}

	/**
	 * Returns name of this stream.
	 */
	@Override
	public String toString() {
		return streamName;
	}

	/**
	 * Returns {@link Fields} which defines tuples sent through this stream.
	 * There is canonical way to construct tuples for streams in our
	 * application. Tuples has the form of (<stream_name>0, <stream_name>1 ...).
	 * General intention is to simplify building topology of application.
	 * 
	 * @return canonical tuples sent through this stream
	 */
	public Fields getFieldsOfThisStream() {
		return fieldsOfThisStream;
	}

}
