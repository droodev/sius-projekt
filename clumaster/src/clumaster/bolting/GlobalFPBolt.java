package clumaster.bolting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import backtype.storm.topology.BasicOutputCollector;
import clumaster.Starter;
import clumaster.bolting.commons.ClumasterStreams;
import clumaster.bolting.commons.DeclaringDispatchingBolt;
import clumaster.model.Clustering;
import clumaster.model.Clusterizer;
import clumaster.model.Point;

/**
 * Calculates final clustering for all set of processed points basing on
 * clusterings calculated by local FP bolts and sent to the bolt on
 * {@link ClumasterStreams#CLUSTERINGS} stream.
 * 
 * @param <T>
 *            type of point processed by this bolt
 */
public abstract class GlobalFPBolt<T extends Number> extends
		DeclaringDispatchingBolt<T> {
	private static final long serialVersionUID = 1L;

	private final Map<Integer, Clustering<T>> clusterings = new HashMap<Integer, Clustering<T>>();
	private Clustering<T> actualClustering;
	@SuppressWarnings("rawtypes")
	protected List<Point> realCenters; 
	protected final Class<T> clazz;

	/**
	 * Constructs bolt which is going to process points of given type.
	 * 
	 * @param clazz
	 *            type of point which is going to be processed
	 * @param realCenters
	 *             centers of real clustering, used for comparison with current clustering
	 */
	@SuppressWarnings("rawtypes")
	public GlobalFPBolt(Class<T> clazz, List<Point> realCenters) {
		this.clazz = clazz;
		this.realCenters = realCenters;
	}

	private List<Point<T>> collectPoints() {
		ArrayList<Point<T>> returnList = new ArrayList<Point<T>>();
		for (Clustering<T> clustering : clusterings.values()) {
			returnList.addAll(clustering.getCenters());
		}
		return returnList;
	}

	/**
	 * When new clustering comes this method actualizes final clustering and
	 * calls {@link #newClusterizationDone(Clustering)} with this new clustering
	 * as parameter argument.
	 * 
	 * @see clumaster.bolting.BaseSimpleStreamDispatchingBolt#executeClustersStream(clumaster.model.Clustering,
	 *      int, backtype.storm.topology.BasicOutputCollector)
	 */
	@Override
	public void executeClustersStream(Clustering<T> newClustering,
			int clusteringSourceTaskId, BasicOutputCollector collector) {
		clusterings.put(clusteringSourceTaskId, newClustering);
		actualClustering = Clusterizer.clusterize(clazz, collectPoints(),
				Starter.CLUSTERING_DEGREE);
		newClusterizationDone(actualClustering);
	}

	/**
	 * Method left to override if one wants to do additional action after
	 * clustering had came up.
	 * 
	 * @param newFinalClustering
	 *            which was recently calculated
	 */
	public abstract void newClusterizationDone(Clustering<T> newFinalClustering);
}
