package clumaster.bolting;

import backtype.storm.topology.BasicOutputCollector;
import clumaster.model.Clustering;
import clumaster.model.Point;

/**
 * Helper base class which implements all abstract methods of
 * {@link SimpleStreamDispatchingBolt} as empty.
 * 
 * @param <T>
 *            type of point processed by this bolt
 */
@SuppressWarnings("serial")
public abstract class BaseSimpleStreamDispatchingBolt<T extends Number> extends
		SimpleStreamDispatchingBolt<T> {

	/**
	 * @{inheritDoc
	 */
	@Override
	public void executeInsertStream(Point<T> inputPoint,
			BasicOutputCollector collector) {
	}

	/**
	 * @{inheritDoc
	 */
	@Override
	public void executeDeleteStream(Point<T> deletedPoint,
			BasicOutputCollector collector) {
	}

	/**
	 * @{inheritDoc
	 */
	@Override
	public void executeClustersStream(Clustering<T> newClustering,
			int clusteringSourceTaskId, BasicOutputCollector collector) {
	}

}
