package clumaster.bolting;

import clumaster.bolting.commons.ClumasterStreams;
import clumaster.model.Clustering;
import clumaster.model.Point;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;

/**
 * Helper class which transforms one method which is responsible for receiving
 * tuples to several, specialized, named versions of them (as in template method
 * pattern). Parameters of those methods are field values extracted from tuples
 * connected with corresponding streams.
 * 
 * @param <T>
 *            type of point processed by this bolt
 */
@SuppressWarnings("serial")
public abstract class SimpleStreamDispatchingBolt<T extends Number> extends
		BaseBasicBolt {

	/**
	 * Applies method pattern to this overriden method. This method just
	 * delegates to additional method (which could be overriden) based on the
	 * stream which this tuple was found on.
	 * 
	 * @see backtype.storm.topology.IBasicBolt#execute(backtype.storm.tuple.Tuple,
	 *      backtype.storm.topology.BasicOutputCollector)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void execute(Tuple input, BasicOutputCollector collector) {
		if (isStream(input, ClumasterStreams.CLUSTERINGS)) {
			executeClustersStream((Clustering<T>) input.getValue(0),
					(int) input.getValue(1), collector);
		} else if (isStream(input, ClumasterStreams.DELETE)) {
			executeDeleteStream((Point<T>) input.getValue(0), collector);
		} else if (isStream(input, ClumasterStreams.INSERT)) {
			executeInsertStream((Point<T>) input.getValue(0), collector);
		} else {
			executeOther(input, collector);
		}
	}

	private boolean isStream(Tuple input, ClumasterStreams streamDefinition) {
		return input.getSourceStreamId().equals(streamDefinition.toString());
	}

	/**
	 * Called when tuple on {@link ClumasterStreams#INSERT} received.
	 * 
	 * @param inputPoint
	 *            extracted from tuple
	 * @param collector
	 *            to use to send output
	 */
	public abstract void executeInsertStream(Point<T> inputPoint,
			BasicOutputCollector collector);

	/**
	 * Called when tuple on {@link ClumasterStreams#DELETE} received.
	 * 
	 * @param inputPoint
	 *            extracted from tuple
	 * @param collector
	 *            to use to send output
	 */
	public abstract void executeDeleteStream(Point<T> deletedPoint,
			BasicOutputCollector collector);

	/**
	 * Called when tuple on {@link ClumasterStreams#CLUSTERINGS} received.
	 * 
	 * @param newClustering
	 *            extracted from tuple
	 * @param clusteringSourceTaskId
	 *            extracted from tuple
	 * @param collector
	 *            to use to send output
	 */
	public abstract void executeClustersStream(Clustering<T> newClustering,
			int clusteringSourceTaskId, BasicOutputCollector collector);

	/**
	 * Called when tuple on any other stream received.
	 * 
	 * @param input
	 *            tuple
	 * @param collector
	 *            to use to send output
	 */
	public void executeOther(Tuple input, BasicOutputCollector collector) {
		System.out.println("Dispatched nowehere: " + input);
	}

}
