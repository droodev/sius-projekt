package clumaster.bolting;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import clumaster.Starter;
import clumaster.model.Clustering;
import clumaster.model.Point;

/**
 * Bolt saves subsequent final clusterings in file.
 * 
 * @param <T>
 *            type of point processed by this bolt
 */
@SuppressWarnings("serial")
public class FileSaverGlobalBolt<T extends Number> extends GlobalFPBolt<T> {

	private String filename;

	/**
	 * Constructs bolt which is going to process points of given type and save
	 * it to file in user's home dir.
	 * 
	 * @param clazz
	 *            type of point which is going to be processed
	 * @param filename
	 *            relative path to file which is pasted after "~/" path
	 * @param realCenters
	 * 			  centers of real clustering, used for comparison with current clustering
	 */
	@SuppressWarnings("rawtypes")
	public FileSaverGlobalBolt(Class<T> clazz, String filename, List<Point> realCenters) {
		super(clazz, realCenters);
		this.filename = filename;

	}

	/**
	 * Saves subsequent final clusterings in file defined in constructor.
	 * 
	 * @see clumaster.bolting.GlobalFPBolt#newClusterizationDone(clumaster.model.Clustering)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void newClusterizationDone(Clustering<T> newFinalClustering) {
		try (FileWriter fileWriter = new FileWriter(Starter.getOutputDir(), true)) {
			fileWriter.write(newFinalClustering.toString());
			fileWriter.write("\n");
			double error = newFinalClustering.getClusteringError(realCenters);
			fileWriter.write("Clustering error: " + error + "\n");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
//		double error = newFinalClustering.getClusteringError(Starter.<T>getChosenCenters(clazz));
//		System.out.println(error);
//		System.out.println(newFinalClustering);
	}
}
