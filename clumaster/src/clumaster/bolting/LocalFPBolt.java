package clumaster.bolting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import clumaster.Starter;
import clumaster.bolting.commons.ClumasterStreamConfiguration;
import clumaster.bolting.commons.ClumasterStreams;
import clumaster.bolting.commons.DeclaringDispatchingBolt;
import clumaster.model.Clustering;
import clumaster.model.Clusterizer;
import clumaster.model.Point;

/**
 * Local bolt which performs operation of local FP clustering and, if local
 * clustering changes, sends resulting clustering to global FP. Bolt maintains
 * deletion of points too.
 * 
 * @param <T>
 *            type of point processed by this bolt
 */
@SuppressWarnings("serial")
public class LocalFPBolt<T extends Number> extends DeclaringDispatchingBolt<T> {
	private float eps = 1.5F;
	private ArrayList<Point<T>> points = new ArrayList<>();
	private Clustering<T> actualClustering;
	private Class<T> clazz;
	private int taskId;

	public LocalFPBolt(Class<T> clazz,
			List<ClumasterStreamConfiguration> streamsConfiguration,
			float eps) {
		super(streamsConfiguration);
		this.clazz = clazz;
		this.eps = eps;
	}

	/**
	 * Saves the task's id of task executing this bolt.
	 * 
	 * @see backtype.storm.topology.base.BaseBasicBolt#prepare(java.util.Map,
	 *      backtype.storm.task.TopologyContext)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void prepare(Map stormConf, TopologyContext context) {
		taskId = context.getThisTaskId();
	}

	private void updateClusteringAndSendUpdate(BasicOutputCollector collector) {
		actualClustering = Clusterizer.clusterize(clazz, points,
				Starter.CLUSTERING_DEGREE);
		collector.emit(ClumasterStreams.CLUSTERINGS.toString(),
				Arrays.<Object> asList(actualClustering, taskId));
	}

	/**
	 * Manages to check if newly inserted point forces clustering changes and do
	 * so if yes and then inform global FP about new clustering on the
	 * {@link ClumasterStreams#CLUSTERINGS} stream.
	 * 
	 * @see clumaster.bolting.BaseSimpleStreamDispatchingBolt#executeInsertStream(clumaster.model.Point,
	 *      backtype.storm.topology.BasicOutputCollector)
	 */
	@Override
	public void executeInsertStream(Point<T> inputPoint,
			BasicOutputCollector collector) {
		// System.out.println(String.format("TID: %d Got point: %s", taskId,
		// inputPoint));
		points.add(inputPoint);
		if (points.size() >= Starter.CLUSTERING_DEGREE) {
			if (actualClustering == null
					|| actualClustering.findMinCenterDistance(inputPoint) >= (1 + eps / 2)
							* actualClustering.getRadius()) {
				// System.out.println("Reclustering...");
				updateClusteringAndSendUpdate(collector);
				// System.out.println("TASK: \n" + actualClustering);
			}
		}

	}

	/**
	 * Manages to check if newly inserted point forces clustering changes (i.e.
	 * deleted point was center of clustering) if yes do so and then inform
	 * global FP about new clustering on the
	 * {@link ClumasterStreams#CLUSTERINGS} stream.
	 * 
	 * @see clumaster.bolting.BaseSimpleStreamDispatchingBolt#executeInsertStream(clumaster.model.Point,
	 *      backtype.storm.topology.BasicOutputCollector)
	 */
	@Override
	public void executeDeleteStream(Point<T> deletedPoint,
			BasicOutputCollector collector) {
		// System.out.println(String.format("TID: %d Got point to delete: %s",
		// taskId, deletedPoint));
		points.remove(deletedPoint);
		if (actualClustering == null) {
			// means that there wasn't enough points to create clustering
			return; // TODO noone should see it:/
		}
		if (actualClustering.isCenter(deletedPoint)) {
			// System.out.println("Deleted center: " + deletedPoint);
			// System.out.println("Reclustering because deleted...");
			updateClusteringAndSendUpdate(collector);
			// System.out.println(actualClustering);
		} else {
			actualClustering.removeNonCenter(deletedPoint);
		}

	}

}
