package clumaster.bolting;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import backtype.storm.generated.Grouping;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import clumaster.bolting.commons.ClumasterStreamConfiguration;
import clumaster.bolting.commons.ClumasterStreams;
import clumaster.bolting.commons.DeclaringDispatchingBolt;
import clumaster.generator.numbers.ComparableNumber;
import clumaster.generator.numbers.LeftClosedRightOpenRange;
import clumaster.model.Point;

/**
 * Bolt responsible for dispensing points to LocalFP nodes.
 * Points are dispensed according to the subspace that they belong.
 * Each LocalFP node is then responsible for it's own subspace.
 * 
 * @param <T> type of point processed by this bolt
 */
public class PointDispenserBolt<T extends Number> 
	extends DeclaringDispatchingBolt<T> {

	private static final long serialVersionUID = 1L;
	
	private Map<Integer, Integer> taskMapping = new HashMap<Integer, Integer>();
	private ArrayList<ArrayList<LeftClosedRightOpenRange<ComparableNumber<T>>>> subspaces = new ArrayList<ArrayList<LeftClosedRightOpenRange<ComparableNumber<T>>>>();
	
	/**
	 * Constructs the PointDispenserBolt with given configuration and subspaces.
	 * 
	 * @param configuration bolt configuration
	 * @param subspaces list of subspaces. Each subspace is represented by a list 
	 * of ranges. Each range corresponds to a point dimension. A subspace is then
	 * a list of ranges for all the coordinates and contains the n-cube that those
	 * ranges constitute. The number of subspaces should be equal to the number of 
	 * {@link LocalFPBolt} nodes. The size of subspaces should be equal to the dimension of
	 * processed points.
	 */
	public PointDispenserBolt(List<ClumasterStreamConfiguration> configuration, 
			List<? extends List<LeftClosedRightOpenRange<ComparableNumber<T>>>> subspaces) {
		super(configuration);
		checkNotNull(subspaces);

		for (Collection<LeftClosedRightOpenRange<ComparableNumber<T>>> subspace : subspaces){
			this.subspaces.add(new ArrayList<>(subspace));
		}
			
	}
	
	/**
	 * Gets the ids of the tasks that consume from this bolt and assigns a subspace.
	 * to each of them.
	 * 
	 * @see backtype.storm.topology.base.BaseBasicBolt#prepare(java.util.Map,
	 *      backtype.storm.task.TopologyContext)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void prepare(Map stormConf, TopologyContext context) {
		Map<String, Map<String, Grouping>> targets = context.getThisTargets();
		List<Integer> targetIds = null; 
		
		Map<String, Grouping> targetsOfInsertStream = targets.get(ClumasterStreams.INSERT.toString());
		
		for (Map.Entry<String, Grouping> component : targetsOfInsertStream.entrySet()){
			targetIds = context.getComponentTasks(component.getKey());
			break;
		}
		
		for(int i = 0; i < targetIds.size(); i++){
			// i is index of subspace in subspaces array
			taskMapping.put(i, targetIds.get(i));
		}
		
	}

	/**
	 * Passes the inserted point to a correct {@link LocalFPBolt} based on the 
	 * subspace that the point is in. 
	 * 
	 * @see clumaster.bolting.BaseSimpleStreamDispatchingBolt#executeInsertStream(clumaster.model.Point,
	 *      backtype.storm.topology.BasicOutputCollector)
	 */
	@Override
	public void executeInsertStream(Point<T> inputPoint,
			BasicOutputCollector collector) {	
		collector.emitDirect(taskMapping.get(subspaceForPoint(inputPoint)), 
				ClumasterStreams.INSERT.toString(),
				Arrays.<Object> asList(inputPoint));
	}

	/**
	 * Passes the deleted point to a correct {@link LocalFPBolt} based on the 
	 * subspace that the point is in. 
	 * 
	 * @see clumaster.bolting.BaseSimpleStreamDispatchingBolt#executeDeleteStream(clumaster.model.Point,
	 *      backtype.storm.topology.BasicOutputCollector)
	 */
	@Override
	public void executeDeleteStream(Point<T> deletedPoint,
			BasicOutputCollector collector) {
		collector.emitDirect(taskMapping.get(subspaceForPoint(deletedPoint)), 
				ClumasterStreams.DELETE.toString(),
				Arrays.<Object> asList(deletedPoint));
		
	}

	private int subspaceForPoint(Point<T> p){
		for(int i = 0; i < subspaces.size(); ++i) {
			if(isInSubspace(p, subspaces.get(i))) {
				return i;
			}
		}
		return -1;
	}

	private boolean isInSubspace(Point <T> p, List<LeftClosedRightOpenRange<ComparableNumber<T>>> subspace) {
		for (int i = 0; i < p.getPointDimension(); i++) {
			if (!subspace.get(i).isInRange(ComparableNumber.createComparable(p.getCoordinate(i)))) {
				return false;
			}
		}
		return true;
	}

}
