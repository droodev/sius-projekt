package clumaster.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

/**
 * It's a point in n-dimensional space which operations are defensive checked.
 * Exceptions are not checked not to obfuscate code.
 * 
 * @param <T>
 *            Type of point value
 */
public class NDPoint<T extends Number> extends DefensiveCheckedPoint<T> implements Serializable{

	private static final long serialVersionUID = 2952400977914959213L;
	private List<T> coordinates;

	static <T extends Number> NDPoint<T> create(int dimension,
			Collection<T> coordinates) {
		return new NDPoint<T>(dimension, new ArrayList<>(coordinates));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getCoordinate(int coordinate) {
		Preconditions.checkArgument(checkCoordinateValidity(coordinate));
		return coordinates.get(coordinate);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Point(");
		for (T coordinate : coordinates) {
			builder.append(coordinate.toString());
			builder.append(", ");
		}
		builder.delete(builder.length() - 2, builder.length());
		builder.append(")");
		return builder.toString();
	}

	private NDPoint(int dimension, List<T> coordinates) {
		super(dimension);
		Preconditions.checkNotNull(coordinates);
		Preconditions.checkArgument(Iterables.all(coordinates,
				Predicates.notNull()));
		this.coordinates = coordinates;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ImmutableList<T> getCoordinates() {
		return ImmutableList.copyOf(coordinates);
	}

}
