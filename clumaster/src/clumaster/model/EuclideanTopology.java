package clumaster.model;

import java.util.Collection;

import org.apache.commons.lang3.ArrayUtils;

import com.google.common.base.Preconditions;

/**
 * Euclidean topology to count simple properties as distance. Class is tricky
 * because of not overloaded operators for {@link Number} subclasses. Uses ugly
 * casting and if..else if..else constructs.
 * 
 * 
 */
public class EuclideanTopology {

	/**
	 * Counts distance between given points. Simple euclidean distance. Methods
	 * could throw {@link IllegalArgumentException} if given arguments do not
	 * match (runtime classes of PointA, PointB and clazz)
	 * 
	 * @param pointA
	 *            first point
	 * @param pointB
	 *            second point
	 * @param clazz
	 *            runtime class of points
	 * @return calculated distance
	 */
	public static <T extends Number> double distanceBetween(Point<T> pointA,
			Point<T> pointB, Class<? extends Number> clazz) {
		Preconditions.checkNotNull(pointA);
		Preconditions.checkNotNull(pointB);
		Preconditions.checkNotNull(clazz);
		Preconditions.checkArgument(pointA.getPointDimension() == pointB
				.getPointDimension());
		Preconditions.checkArgument(areTypesConsistentAndValid(pointA, pointB,
				clazz));

		return objectWiseCalculateDistance(pointA.getCoordinates(),
				pointB.getCoordinates(), clazz);

	}

	@SuppressWarnings("unchecked")
	private static <T> double objectWiseCalculateDistance(
			Collection<T> collection, Collection<T> collection2,
			Class<? extends Number> clazz) {
		Preconditions.checkNotNull(collection);
		Preconditions.checkNotNull(collection2);
		Preconditions.checkNotNull(clazz);

		double distanceToReturn = -1;
		if (Integer.class.isAssignableFrom(clazz)) {
			distanceToReturn = calculateDistance(
					intObjectsToIntPrimitives((Collection<Integer>) collection),
					intObjectsToIntPrimitives((Collection<Integer>) collection2));
		} else if (Float.class.isAssignableFrom(clazz)) {
			distanceToReturn = calculateDistance(
					floatObjectsToFloatPrimitives((Collection<Float>) collection),
					floatObjectsToFloatPrimitives((Collection<Float>) collection2));
		} else {
			throw new IllegalArgumentException(String.format(
					"Operation for this type (%s) is not supported", clazz));
		}
		return distanceToReturn;
	}

	private static float[] floatObjectsToFloatPrimitives(
			Collection<Float> floatObjects) {
		Float[] wrappers = new Float[floatObjects.size()];
		floatObjects.toArray(wrappers);
		return ArrayUtils.toPrimitive(wrappers);
	}

	private static int[] intObjectsToIntPrimitives(
			Collection<Integer> intObjects) {
		Integer[] wrappers = new Integer[intObjects.size()];
		intObjects.toArray(wrappers);
		return ArrayUtils.toPrimitive(wrappers);
	}

	private static double calculateDistance(int[] aPoints, int[] bPoints) {
		int dimension = aPoints.length;
		long sumOfSquares = 0;
		for (int i = 0; i < dimension; ++i) {
			long distanceBeetweenCoordinates = Math
					.abs(aPoints[i] - bPoints[i]);
			sumOfSquares += distanceBeetweenCoordinates
					* distanceBeetweenCoordinates;
		}
		return Math.sqrt(sumOfSquares);
	}

	private static double calculateDistance(float[] aPoints, float[] bPoints) {
		int dimension = aPoints.length;
		double sumOfSquares = 0;
		for (int i = 0; i < dimension; ++i) {
			double distanceBeetweenCoordinates = Math.abs(aPoints[i]
					- bPoints[i]);
			sumOfSquares += distanceBeetweenCoordinates
					* distanceBeetweenCoordinates;
		}
		return Math.sqrt(sumOfSquares);
	}

	private static <T extends Number> boolean areTypesConsistentAndValid(
			Point<T> pointA, Point<T> pointB, Class<? extends Number> clazz) {
		return clazz.isAssignableFrom(pointA.getCoordinate(0).getClass())
				&& clazz.isAssignableFrom(pointB.getCoordinate(0).getClass());
	}
}
