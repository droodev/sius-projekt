package clumaster.model;

import java.util.Collection;

/**
 * Builder for {@link NDPoint} classes. It should be used by clients, instead of
 * using {@link NDPoint#create} factory method!
 * 
 * @param <T>
 *            Type of point value
 */
public class NDPointBuilder<T extends Number> {

	/**
	 * Factory method which creates {@link NDPointBuilder}.
	 * 
	 * @param dimension
	 *            of created point
	 * @return created, new builder of point
	 */
	public static <T extends Number> NDPointBuilder<T> create(int dimension) {
		return new NDPointBuilder<T>(dimension);
	}

	private final InitializationTimeMutablePoint<T> initializationTimePointSeed;

	private NDPointBuilder(int dimension) {
		initializationTimePointSeed = new InitializationTimeMutablePoint<>(
				dimension);
	}

	/**
	 * Sets given coordinate of created point to given value.
	 * 
	 * @param coordinate
	 *            which should hold value
	 * @param value
	 *            which should be hold by coordinate
	 * @return this, for flueant API
	 */
	public NDPointBuilder<T> setCoordinate(int coordinate, T value) {
		initializationTimePointSeed.setCoordinate(coordinate, value);
		return this;
	}

	/**
	 * Build complete point. This method checks state of builded point and would
	 * throw {@link IllegalStateException} if one wants to build not properly
	 * initialized point.
	 * 
	 * @return built point
	 */
	public Point<T> build() {
		Collection<T> coordinatesList = null;
		try {
			coordinatesList = initializationTimePointSeed
					.getCoordinates();
		} catch (IllegalStateException e) {
			throw new IllegalStateException(
					"Tried to build uninitialized point! This cannot be done",
					e);
		}
		return NDPoint.create(initializationTimePointSeed.getPointDimension(),
				coordinatesList);
	}
}
