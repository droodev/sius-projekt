package clumaster.model;

import java.io.Serializable;

import com.google.common.base.Preconditions;

/**
 * Defensive subclass of point which does necessary checks and provides
 * defensive checking methods for subclasses.
 * 
 * @param <T>
 *            Type of point value
 */
public abstract class DefensiveCheckedPoint<T extends Number> implements Point<T>, Serializable {
	
	private static final long serialVersionUID = -1633998804141807810L;
	protected int dimension;

	protected DefensiveCheckedPoint(int dimension) {
		Preconditions.checkArgument(dimension > 0);
		this.dimension = dimension;
	}

	protected boolean checkCoordinateValidity(int coordinate) {
		return coordinate < getPointDimension() && coordinate >= 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPointDimension() {
		return dimension;
	}
}
