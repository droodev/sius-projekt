package clumaster.model;

import java.util.ArrayList;
import java.util.Collection;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

/**
 * Represents Point class at the initialization time. It's worth to mention that
 * this point could have inconsistent state so can't be used as regular
 * {@link Point}. It's created to support {@link NDPointBuilder} where this
 * class is used to implement fluent API.
 * 
 * @param <T>
 *            Type of point value
 */
class InitializationTimeMutablePoint<T extends Number> extends DefensiveCheckedPoint<T> {

	private static final long serialVersionUID = 6945515458051557244L;
	private final ArrayList<Optional<T>> coordinatesList = new ArrayList<>();

	InitializationTimeMutablePoint(int dimension) {
		super(dimension);
		initializeCoordinatesList();
	}

	private void initializeCoordinatesList() {
		for (int i = 0; i < coordinatesList.size(); ++i) {
			coordinatesList.add(Optional.<T> absent());
		}
	}

	void setCoordinate(int coordinate, T value) {
		Preconditions
				.checkArgument(
						checkCoordinateValidity(coordinate),
						String.format(
								"Cannot set coordinate %d if point has dimension of %d",
								coordinate, dimension));
		coordinatesList.add(coordinate, Optional.of(value));
	}

	boolean isFullyInitialized() {
		return Iterables.all(coordinatesList,
				Predicates.not(Predicates.equalTo(Optional.<T> absent())));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ImmutableList<T> getCoordinates() {
		Preconditions.checkState(isFullyInitialized(),
				"Cannot get uninitialized coordinates list of point");
		Collection<T> returnedCollection = Collections2.transform(
				coordinatesList, new Function<Optional<T>, T>() {

					@Override
					public T apply(Optional<T> arg0) {
						return arg0.get();
					}

				});
		return ImmutableList.copyOf(returnedCollection);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T getCoordinate(int coordinate) {
		Preconditions.checkArgument(checkCoordinateValidity(coordinate));
		return coordinatesList.get(coordinate).get();
	}

}
