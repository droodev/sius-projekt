package clumaster.model;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

/**
 * Class encapsulates simple clustering algorithm - Farthest Point. Class cannot
 * be instantiated out of it's body - private scoped constructor.
 * 
 * @param <T>
 *            Type of point values
 */
public class Clusterizer<T extends Number> {
	/**
	 * Create ordered clustering using FP algorithm.
	 * 
	 * @param clazz
	 *            representing runtime type of point values
	 * @param pointsToClusterize
	 *            input points to create clustering for
	 * @param numberOfCenters
	 *            degree of clustering
	 * @return correct clustering for this point
	 */
	public static <T extends Number> Clustering<T> clusterize(Class<T> clazz,
			List<Point<T>> pointsToClusterize, int numberOfCenters) {
		return new Clusterizer<T>(clazz, pointsToClusterize, numberOfCenters)
				.countClustering();
	}

	private Class<T> clazz;
	private List<Point<T>> pointsToClusterize;
	private int numberOfCenters;

	private Clusterizer(Class<T> clazz, List<Point<T>> pointsToClusterize,
			int numberOfCenters) {
		this.clazz = clazz;
		this.pointsToClusterize = pointsToClusterize;
		this.numberOfCenters = numberOfCenters;
	}

	private Clustering<T> countClustering() {
		List<Point<T>> workingPointsList = new ArrayList<>(
				this.pointsToClusterize);
		Preconditions.checkNotNull(workingPointsList);
		Preconditions
				.checkArgument(workingPointsList.size() >= numberOfCenters);
		ArrayList<Point<T>> centersList = new ArrayList<>();
		centersList.add(workingPointsList.get(0));
		workingPointsList.remove(0);
		for (int i = 0; i < numberOfCenters - 1; ++i) {
			Point<T> newCenter = findNewCenter(centersList);
			centersList.add(newCenter);
			workingPointsList.remove(newCenter);
		}
		return Clustering.createClustering(numberOfCenters, centersList, clazz,
				pointsToClusterize);
	}

	private Point<T> findNewCenter(ArrayList<Point<T>> centers) {
		int maximalIndex = -1;
		double maximalValue = -1;
		for (Point<T> point : pointsToClusterize) {
			double minDiameter = findMinCenterDistance(point, centers, clazz);
			if (maximalValue < minDiameter) {
				maximalValue = minDiameter;
				maximalIndex = pointsToClusterize.indexOf(point);
			}
		}
		return pointsToClusterize.get(maximalIndex);
	}

	/**
	 * Calculates distance to the closest center in given centers list.
	 * 
	 * @param point
	 *            to measure distance to closest center of
	 * @param centers
	 *            which are considered in calculation
	 * @param clazz
	 *            which represents runtime type of point values
	 * @return distance to the closes center
	 * @throws IllegalArgumentException
	 *             if cardinality of centers is not greater than 0
	 */
	public static <T extends Number> double findMinCenterDistance(
			Point<T> point, List<Point<T>> centers, Class<T> clazz) {
		Preconditions.checkNotNull(point);
		Preconditions.checkNotNull(centers);
		Preconditions.checkNotNull(clazz);
		Preconditions.checkState(centers.size() > 0);
		double minimalValue = -1;
		for (Point<T> center : centers) {
			double minimalValueCandidate;
			final double distanceBetween = EuclideanTopology.distanceBetween(
					point, center, clazz);
			if (minimalValue == -1) {
				minimalValueCandidate = distanceBetween;
			} else {
				minimalValueCandidate = Math.min(minimalValue, distanceBetween);
			}
			minimalValue = minimalValueCandidate;
		}
		return minimalValue;
	}
}
