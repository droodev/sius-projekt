package clumaster.model;

import com.google.common.collect.ImmutableList;

/**
 * Represents arbitrarily point. That means that point can have any dimension
 * and be composed of any type.
 * 
 * @param <T>
 *            Type of point value - generally it could be everything but for
 *            this project it's constrained to Number subclasses
 */
public interface Point<T extends Number> {
	/**
	 * Gets dimension of the point. Beware of returning here something less or
	 * equal 0 - could be confusing for client!
	 * 
	 * @return dimension of the point
	 */
	public int getPointDimension();

	/**
	 * Gets value of given coordinate;
	 * 
	 * @param coordinate
	 *            of which value is ordered
	 * @return value of coordinate
	 */
	public T getCoordinate(int coordinate);

	/**
	 * 
	 * Gets all coordinates. The contract is that iterator of that collection
	 * should preserve correct coordinates order
	 * 
	 * @return all coordinates in collection
	 */
	public ImmutableList<T> getCoordinates();
}
