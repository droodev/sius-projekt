package clumaster.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/**
 * Clustering of points. This clustering holds invariant of radius. One can do
 * anything that he or she wants to do except to operations which can change
 * radius. Especially one can delete non-center point.
 * 
 * @param <T>
 *            Values of clustered points
 */
public class Clustering<T extends Number> implements Serializable {
	private static final long serialVersionUID = 1L;
	private int degree;
	private List<Point<T>> centers;
	private Class<T> clazz;
	private List<Point<T>> points;
	private double radius;

	public double getRadius() {
		return radius;
	}
	
	private Clustering(int degree, List<Point<T>> centres, Class<T> clazz,
			List<Point<T>> points) {
		Preconditions.checkNotNull(centres);
		Preconditions.checkNotNull(clazz);
		Preconditions.checkNotNull(points);
		Preconditions.checkArgument(degree == centres.size());
		this.clazz = clazz;
		this.degree = degree;
		this.centers = new ArrayList<>(centres);
		this.points = new ArrayList<>(points);
		radius = findRadius();
	}

	/**
	 * Return degree of clustering - number of centers.
	 * 
	 * @return degree of clustering
	 */
	public int getDegree() {
		return degree;
	}

	/**
	 * Returns centers of clustering.
	 * 
	 * @return centers as {@link ImmutableList} not to allow state corrupting
	 */
	public ImmutableList<Point<T>> getCenters() {
		return ImmutableList.<Point<T>> builder().addAll(centers).build();
	}

	/**
	 * Creates clustering with given parameters. This method doesn't count any
	 * clustering. For this purpose {@link Clusterizer} is present.
	 * 
	 * @param degree
	 *            of clustering
	 * @param centers
	 *            of clustering
	 * @param clazz
	 *            runtime class of point values
	 * @param points
	 *            list of clustered points
	 * @return new clustering
	 */
	public static <T extends Number> Clustering<T> createClustering(int degree,
			ArrayList<Point<T>> centers, Class<T> clazz, List<Point<T>> points) {
		return new Clustering<T>(degree, centers, clazz, points);
	}

	/**
	 * Checks if given point is center of this clustering.
	 * 
	 * @param point
	 *            to check
	 * @return whether given point is center of clustering
	 */
	public boolean isCenter(Point<T> point) {
		Preconditions.checkNotNull(point);
		return centers.contains(point);
	}

	/**
	 * Gets number of points which are clustered by this clustering. That is
	 * number of input points for this clustering.
	 * 
	 * @return number of input points, cardinality of clustering
	 */
	public int getClusteredCardinality() {
		return points.size();
	}

	/**
	 * Removes one point which cannot be center. As such removal holds invariant
	 * of radius - could be safely done.
	 * 
	 * @param point
	 *            to remove
	 * @throws IllegalArgumentException
	 *             if given point is center of this clustering
	 */
	public void removeNonCenter(Point<T> point) {
		Preconditions.checkNotNull(point);
		Preconditions.checkArgument(!isCenter(point));
		points.remove(point);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(String.format("%d-clustering with centres:\n", degree));
		Joiner.on(",").appendTo(builder, centers);
		builder.append("\nwith radius: " + radius + " and cardinality: "
				+ getClusteredCardinality());
		return builder.toString();
	}

	private double findRadius() {
		double maxRadius = -1;
		for (Point<T> point : points) {
			double minDiameter = Clusterizer.findMinCenterDistance(point,
					centers, clazz);
			maxRadius = Math.max(maxRadius, minDiameter);
		}
		return maxRadius;
	}

	/**
	 * Calculates distance to the closes center. This method uses
	 * {@link Clusterizer#clusterize(Class, List, int)} static method passing
	 * appropriate arguments. It's implemented only for comfortable API usage.
	 * 
	 * @see Clusterizer
	 * @param point
	 *            to measure distance to closest center of
	 * @return distance to the closest center
	 * 
	 */
	public double findMinCenterDistance(Point<T> point) {
		return Clusterizer.findMinCenterDistance(point, centers, clazz);
	}
	
	/**
	 * Calculates the error of this clustering with regards to actual centers.
	 * 
	 * @param actualCenters the correct centers that should have been calculated
	 * @return error of clustering
	 */
	@SuppressWarnings("rawtypes")
	public double getClusteringError(List<Point> actualCenters) {
		double distance;
		double [][] distances = new double[centers.size()][actualCenters.size()];
		int i = 0;
		int j;
		for(Point<T> cc : centers) {
			j = 0;
			for(Point<T> ac : actualCenters) {		
				distance = EuclideanTopology.distanceBetween(cc, ac, clazz);
				distances[i][j] = distance;
				j++;
			}
			i++;
		}
		
		return Math.sqrt(minDistance(distances) / centers.size());
	}
	
	private double minDistance(double[][] distances) {
		Map<Integer, Integer> chosen = new HashMap<Integer, Integer> ();
		return minDistance(chosen, distances, 0);
	}

	private double minDistance(Map<Integer, Integer> chosen, double[][] distances, double distance) {
		int i = chosen.size();
		while(chosen.containsKey(i)){
			i++;
		}
		if(i == distances.length) {
			return distance;
		}
		double min = -1;
		for(int j = 0; j < distances.length; j++) {
			if(!chosen.containsKey(j)) {
				chosen.put(j, i);
				double thisDistance = distances[i][j];
				double currentdistance = minDistance(chosen, distances, distance + thisDistance * thisDistance);
				if (currentdistance < min || min < 0) {
					min = currentdistance;
				}
				chosen.remove(j);
			}
		}
		return min;
	}
}
