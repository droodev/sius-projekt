package clumaster.spouting;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import com.google.common.base.Preconditions;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import clumaster.bolting.commons.ClumasterStreams;
import clumaster.generator.Generator;
import clumaster.model.Point;

/**
 * Spout which generates points and sends them to the
 * {@link ClumasterStreams#INSERT} as default. As default spout generates spout
 * as fast as possible
 * 
 * @param <T>
 *            represents type of coordinate of generated point
 */
@SuppressWarnings("serial")
public class GeneratorSpout<T extends Number> extends BaseRichSpout {

	private SpoutOutputCollector collector;
	private final Generator<Point<T>> pointGenerator;

	/**
	 * Constructs this type of spout. Spout uses given generator to produce
	 * emitted tuples.
	 * 
	 * @param pointGenerator
	 *            generator which will be used to create points to emitted
	 *            tuples
	 */
	public GeneratorSpout(Generator<Point<T>> pointGenerator) {
		this.pointGenerator = Preconditions.checkNotNull(pointGenerator);
	}

	/**
	 * Overrides. Only stores collector in private field.
	 * 
	 * @see backtype.storm.spout.ISpout#open(java.util.Map,
	 *      backtype.storm.task.TopologyContext,
	 *      backtype.storm.spout.SpoutOutputCollector)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		this.collector = collector;
	}

	/**
	 * Overrides. Sends tuple to {@link ClumasterStreams#INSERT} stream which is
	 * consisted of fields as defined in
	 * {@link #declareOutputFields(OutputFieldsDeclarer)}
	 * 
	 * @see backtype.storm.spout.ISpout#nextTuple()
	 */
	@Override
	public void nextTuple() {
		collector.emit(ClumasterStreams.INSERT.toString(),
				Arrays.<Object> asList(pointGenerator.getNextPoint()),
				UUID.randomUUID());
	}

	/**
	 * Overrides. Declares one field called "Point" on {@link ClumasterStreams#INSERT} stream.
	 * @see backtype.storm.topology.IComponent#declareOutputFields(backtype.storm.topology.OutputFieldsDeclarer)
	 */
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declareStream(ClumasterStreams.INSERT.toString(), new Fields(
				"Point"));
	}

}
